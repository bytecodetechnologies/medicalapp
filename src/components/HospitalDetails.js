import React, { Component } from "react";
import { ButtonContainer } from "./Button";
import { Link } from "react-router-dom";

export default class HospitalDetails extends Component {
  render() {
    return (
      <div>
        <div class="grid-container">
          <div>
            <Link to="/floorwise">
              <ButtonContainer className="button-tiles">
                Floor-wise
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/deptwise">
              <ButtonContainer className="button-tiles">
                Department-wise
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/staffwise">
              <ButtonContainer className="button-tiles">
                Staff-wise
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/patientwise">
              <ButtonContainer className="button-tiles">
                Patient-wise
              </ButtonContainer>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
