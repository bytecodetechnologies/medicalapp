import React from "react";
import AutoCompleteText from "./AutoCompleteText";
import healthproblems from "./Data/healthproblems";
import { Link } from "react-router-dom";
import Title from "./Title";
import DatePick from "./DatePick";
import DropDown from "./Dropdown";
import CheckupCheckbox from "./CheckupCheckbox";
import "./NewAppointment.css";

class NewAppointment extends React.Component {
  state = {};
  render() {
    return (
      <div>
        <Title name="Create an" title="Appointment" />
        <div className="App-Component">
          <label>Health Problem</label>
          <AutoCompleteText items={healthproblems} />
          <br />
          <label className="pushRight">
            Appointment Time: <DatePick />
          </label>
          <label>
            Select a Doctor: <DropDown />
          </label>
          <br />
          <label>Checkup Type: </label>
          <CheckupCheckbox />
          <br />
          <input type="submit" value="Create Appointment" />
          <br />
          <Link to="/appointmentdetails">
            Already have an appointment?
          </Link>
        </div>
      </div>
    );
  }
}

export default NewAppointment;
