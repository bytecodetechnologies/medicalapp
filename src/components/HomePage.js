import React, { Component } from "react";
import { ButtonContainer } from "./Button";
import { Link } from "react-router-dom";

export default class HomePage extends Component {
  render() {
    return (
      <div>
        <div class="grid-container">
          <div>
            <Link to="/newappointment">
              <ButtonContainer className="button-tiles">
                Create a new Appointment
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/doctordetails">
              <ButtonContainer className="button-tiles">
                View Doctor Details
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/checkinout">
              <ButtonContainer className="button-tiles">
                Staff Check-In/Out Record
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/hospitaldetails">
              <ButtonContainer className="button-tiles">
                View Hospital Details
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/pharmacy">
              <ButtonContainer className="button-tiles">
                Pharmacy
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/root">
              <ButtonContainer className="button-tiles">Notes</ButtonContainer>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
