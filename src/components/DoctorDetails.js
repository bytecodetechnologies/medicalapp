import React, { Component } from "react";
import logo from "../logo.svg";
import { ButtonContainer } from "./Button";
import { Link } from "react-router-dom";

export default class DoctorDetails extends Component {
  render() {
    return (
      <div>
        <div class="grid-container">
          <div>
            <Link to="/rabindrathakur">
              <ButtonContainer className="button-tiles">
                <img src={logo} alt="store" className="navbar-brand"></img>
                Dr. Rabindra Thakur
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/kamalasharma">
              <ButtonContainer className="button-tiles">
                <img src={logo} alt="store" className="navbar-brand"></img>
                Dr. Kamala Sharma
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/shafaljha">
              <ButtonContainer className="button-tiles">
                <img src={logo} alt="store" className="navbar-brand"></img>
                Dr. Shafal Jha
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/sabitakc">
              <ButtonContainer className="button-tiles">
                <img src={logo} alt="store" className="navbar-brand"></img>
                Dr. Sabita K.C.
              </ButtonContainer>
            </Link>
          </div>

          <div>
            <Link to="/manishshrestha">
              <ButtonContainer className="button-tiles">
                <img src={logo} alt="store" className="navbar-brand"></img>
                Dr. Manish Shrestha
              </ButtonContainer>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
