import React, { Component } from "react";

export default class ManageAppointment extends Component {
  render() {
    return (
      <div>
        This page is used to manage the appointment as per the doctor's schedule
        and patient's request.
      </div>
    );
  }
}
