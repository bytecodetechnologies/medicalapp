import React, { Component } from "react";
import ChatListComponent from "../chatlist/chatlist";
import ChatViewComponent from "../chatview/chatview";
import ChatTextBoxComponent from "../chatTextBox/chatTextBox";
import NewChatComponent from "../newchat/newChat";
import styles from "./styles";
import { Button, withStyles } from "@material-ui/core";

const firebase = require("firebase");

class DashboardComponent extends Component {
  state = {
    //index of chat that we are currently on
    selectedChat: null,
    //form that we will have when we want a new chat with fried we dont already have a chat with
    newChatFormVisible: false,
    //email of the user that is currently logged in
    email: null,
    //array
    chats: [],
  };
  render() {
    const { classes } = this.props;
    return (
      //we are not rendering this ChatList Component with the router. So we dont have access to prop "history".
      //here using the history props of dashboard since it has one
      <div>
        <ChatListComponent
          history={this.props.history}
          newChatBtnFunc={this.newChatBtnClicked}
          selectChatFunc={this.selectChat}
          chats={this.state.chats}
          userEmail={this.state.email}
          selectedChatIndex={this.state.selectedChat}
        ></ChatListComponent>
        {this.state.newChatFormVisible ? null : (
          <ChatViewComponent
            user={this.state.email}
            chat={this.state.chats[this.state.selectedChat]}
          ></ChatViewComponent>
        )}
        {this.state.selectedChat !== null && !this.state.newChatFormVisible ? (
          <ChatTextBoxComponent
            messageReadFunc={this.messageRead}
            submitMessageFunc={this.submitMessage}
          ></ChatTextBoxComponent>
        ) : null}
        {this.state.newChatFormVisible ? (
          <NewChatComponent
            goToChatFunc={this.goToChat}
            newChatSubmitFunc={this.newChatSubmit}
          ></NewChatComponent>
        ) : null}
        <Button className={classes.signOutBtn} onClick={this.signOut}>
          Sign Out
        </Button>
      </div>
    );
  }

  signOut = () => firebase.default.auth().signOut();

  selectChat = async (chatIndex) => {
    await this.setState({ selectedChat: chatIndex, newChatFormVisible: false });
    this.messageRead();
  };

  submitMessage = (msg) => {
    const docKey = this.buildDocKey(
      this.state.chats[this.state.selectedChat].users.filter(
        (_user) => _user !== this.state.email
      )[0] //since filter return an array we can access it by index 0
    );
    firebase.default
      .firestore()
      .collection("chats")
      .doc(docKey)
      .update({
        messages: firebase.default.firestore.FieldValue.arrayUnion({
          sender: this.state.email,
          message: msg,
          timestamp: Date.now(),
        }),
        receiverHasRead: false,
      });
  };

  //function needed to submit a message
  buildDocKey = (friend) => [this.state.email, friend].sort().join(":"); //[current User that is loggedIn, friend]

  newChatBtnClicked = () =>
    this.setState({ newChatFormVisible: true, selectedChat: null });

  messageRead = () => {
    const docKey = this.buildDocKey(
      this.state.chats[this.state.selectedChat].users.filter(
        (_user) => _user !== this.state.email
      )[0]
    );
    if (this.clickedChatWhereNotSender(this.state.selectedChat)) {
      firebase.default
        .firestore()
        .collection("chats")
        .doc(docKey)
        .update({ receiverHasRead: true });
    } else {
      console.log("Clicked msg where the user was the sender");
    }
  };

  goToChat = async (docKey, msg) => {
    const usersInChat = docKey.split(":");
    const chat = this.state.chats.find((_chat) =>
      usersInChat.every((_user) => _chat.users.includes(_user))
    );
    this.setState({ newChatFormVisible: false });
    await this.selectChat(this.state.chats.indexOf(chat));
    this.submitMessage(msg);
  };

  newChatSubmit = async (chatObj) => {
    const docKey = this.buildDocKey(chatObj.sendTo);
    await firebase.default
      .firestore()
      .collection("chats")
      .doc(docKey)
      .set({
        receiverHasRead: false,
        users: [this.state.email, chatObj.sendTo],
        messages: [
          {
            message: chatObj.message,
            sender: this.state.email,
          },
        ],
      });
    this.setState({ newChatFormVisible: false });
    this.selectChat(this.state.chats.length - 1); //so we can go to latest chat
  };

  clickedChatWhereNotSender = (chatIndex) =>
    this.state.chats[chatIndex].messages[
      this.state.chats[chatIndex].messages.length - 1 //will give us the very very last msg
    ].sender !== this.state.email; //so that receiverHasRead will be set to true only when the receiver has read the msg not sender

  componentDidMount = () => {
    firebase.default.auth().onAuthStateChanged(async (_user) => {
      if (!_user) this.props.history.push("./login");
      else {
        await firebase.default
          .firestore()
          .collection("chats")
          .where("users", "array-contains", _user.email)
          .onSnapshot(async (result) => {
            const chats = result.docs.map((_doc) => _doc.data());
            await this.setState({
              email: _user.email,
              chats: chats,
            });
            console.log(this.state);
          });
      }
    });
  };
}

export default withStyles(styles)(DashboardComponent);
