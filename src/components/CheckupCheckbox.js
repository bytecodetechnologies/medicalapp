import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

class CheckupCheckbox extends Component {
  state = {
    isGeneral: false,
    isXray: false,
    isFullBody: false,
  };

  // Checkbox Initial State
  state = {
    isGeneral: false,
    isXray: false,
    isFullBody: false,
  };

  // React Checkboxes onChange Methods
  onChangeSedan = () => {
    this.setState((initialState) => ({
      isGeneral: !initialState.isGeneral,
    }));
  };

  onChangeSUV = () => {
    this.setState((initialState) => ({
      isXray: !initialState.isXray,
    }));
  };

  onChangeCompact = () => {
    this.setState((initialState) => ({
      isFullBody: !initialState.isFullBody,
    }));
  };

  render() {
    return (
      <div className="App">
        <form>
          <div className="form-check">
            <label className="form-check-label">
              <input
                type="checkbox"
                checked={this.state.isGeneral}
                onChange={this.onChangeSedan}
                className="form-check-input"
              />
              General Checkup
            </label>
          </div>

          <div className="form-check">
            <label className="form-check-label">
              <input
                type="checkbox"
                checked={this.state.isXray}
                onChange={this.onChangeSUV}
                className="form-check-input"
              />
              X-ray checkup
            </label>
          </div>

          <div className="form-check">
            <label className="form-check-label">
              <input
                type="checkbox"
                checked={this.state.isFullBody}
                onChange={this.onChangeCompact}
                className="form-check-input"
              />
              Full Body Checkup
            </label>
          </div>
        </form>
      </div>
    );
  }
}

export default CheckupCheckbox;
