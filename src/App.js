import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import HomePage from "./components/HomePage";
import NavBar from "./components/NavBar";
import NewAppointment from "./components/NewAppointment";
import DoctorDetails from "./components/DoctorDetails";
import MedicineDetails from "./components/MedicineDetails";
import HospitalDetails from "./components/HospitalDetails";
import AppointmentDetails from "./components/AppointmentDetails";
import ManageAppointment from "./components/ManageAppointment";
import Root from "./components/Notepad/Root";
import LoginComponent from "./components/login/login";
import SignupComponent from "./components/signup/signup";
import DashboardComponent from "./components/Chat/dashboard/dashboard";
import CheckInOut from "./components/CheckInOut";
import FloorWise from "./components/HospitalDetails/FloorWise";
import DepartmentWise from "./components/HospitalDetails/DepartmentWise";
import StaffWise from "./components/HospitalDetails/StaffWise";
import PatientWise from "./components/HospitalDetails/PatientWise";
import Pharmacy from "./components/Pharmacy/Pharmacy";
import Details from "./components/Pharmacy/Details";
import NoteTaking from "./components/NoteTaking";

const App = () => {
  return (
    <div className="App">
      <Router>
        <NavBar />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/newappointment" component={NewAppointment} />
          <Route path="/doctordetails" component={DoctorDetails} />
          <Route path="/checkinout" component={CheckInOut} />
          <Route path="/medicinedetails" component={MedicineDetails} />
          <Route path="/hospitaldetails" component={HospitalDetails} />
          <Route path="/appointmentdetails" component={AppointmentDetails} />
          <Route path="/manageappointment" component={ManageAppointment} />
          <Route path="/root" component={Root} />
          <Route path="/login" component={LoginComponent} />
          <Route path="/signup" component={SignupComponent} />
          <Route path="/dashboard" component={DashboardComponent} />
          <Route path="/floorwise" component={FloorWise} />
          <Route path="/deptwise" component={DepartmentWise} />
          <Route path="/staffwise" component={StaffWise} />
          <Route path="/patientwise" component={PatientWise} />
          <Route path="/pharmacy" component={Pharmacy} />
          <Route path="/details" component={Details} />
          <Route path="/notetaking" component={NoteTaking} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
